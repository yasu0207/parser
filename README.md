# parser

`java -jar parser.jar -f [filename] -d [dirname]`のように使ってください。

例えば、データファイルが`valid.json`で出力先ディレクトリが`valid`ならば、

`java -jar parser.jar -f valid.json -d valid`

という感じです。`valid`というディレクトリはあらかじめ作っておいてください。

# requirement
Java 1.8
